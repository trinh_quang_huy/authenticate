#!/usr/bin/env python
#coding:utf-8
"""
  Author:  Thanh Dancer --<>
  Purpose: 
  Created: 09/15/2015
"""
from email.header import decode_header
import email
import base64
import time
import re
import os

########################################################################
class GoogleEmail:
    """"""
    
    #----------------------------------------------------------------------
    def __init__(self, message_raw):
        """Constructor"""
        self.message = email.message_from_string(base64.urlsafe_b64decode(message_raw.encode('ASCII')))
    
    #----------------------------------------------------------------------
    def _delete_multiline(self, input_str, delquote=True):
        """"""
        input_str = input_str.replace("?==?", "?= =?")
        input_str = re.sub(r'\t',' ', input_str)
        if delquote:
            return re.sub(r'[\n|\r|\r\n|\n\r|"]', '', input_str)
        return re.sub(r'[\n|\r|\r\n|\n\r]', '', input_str)        
    
    #----------------------------------------------------------------------
    def _convert_date_unix(self, datestr):

        # make a month dictionary
        month_dict = {
            '01' : 'Jan',
            '02' : 'Feb',
            '03' : 'Mar',
            '04' : 'Apr',
            '05' : 'May',
            '06' : 'Jun',
            '07' : 'Jul',
            '08' : 'Aug',
            '09' : 'Sep',
            '10' : 'Oct',
            '11' : 'Nov',
            '12' : 'Dec',
        }
        
        date_group = re.search(r'^(.+,\s+)?(\d+\s+)([A-Za-z0-9]+)(.+)$', datestr)
        month = date_group.group(3)
        if date_group.group(3) in month_dict.keys():
            month = month_dict[date_group.group(3)]
    
        if date_group.group(1) == None:
            return email.utils.mktime_tz(email.utils.parsedate_tz(str(date_group.group(2)) + month + str(date_group.group(4))))
        return email.utils.mktime_tz(email.utils.parsedate_tz(str(date_group.group(1)) + str(date_group.group(2)) + month + str(date_group.group(4))))
            
       
    #----------------------------------------------------------------------
    def extract_header(self):
        """"""
        header_list = {
            'from' : '',
            'to' : [],
            'subject' : '',
            'bcc' : [],
            'cc' : [],
            'date' : '',
        }
        for header in self.message._headers:
            if header[0] == "From":        
                for tmp in decode_header(self._delete_multiline(header[1].strip(), True)): #From
                    header_list['from'] += tmp[0] + " "
            if header[0] == "To":                
                for tmp in decode_header(self._delete_multiline(header[1].strip(), True)): #To
                    header_list['to'].append(tmp[0])
            if header[0] == "Subject":
                for tmp in decode_header(self._delete_multiline(header[1].strip(), True)): #Subject
                    header_list['subject'] += tmp[0] + " "
            if header[0] == "Bcc":
                for tmp in decode_header(self._delete_multiline(header[1].strip(), True)): #BCC
                    header_list['bcc'].append(tmp[0])
            if header[0] == "Cc":
                for tmp in decode_header(self._delete_multiline(header[1].strip(), True)): #CC
                    header_list['cc'].append(tmp[0])
            if header[0] == "Date":
                header_list['date'] = self._convert_date_unix(header[1])
        return header_list
    
    #----------------------------------------------------------------------
    def save_attachments(self, location, prefix=None):
        """"""
        attachment_list = []
        # Loop each part of message        
        for part in self.message.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            
            # If the part has filename field
            if part.get_filename():                
                # Because attachment's name will be Vietnamese, Japanese or Chinese...
                # So we must convert it to UTF-8                
                try:
                    # The first time, encode it to utf-8
                    filename = decode_header(self._delete_multiline(part.get_filename()))[0][0].encode('utf-8', 'replace')
                except:
                    try:
                        # The second time, encode it to utf-8
                        filename = part.get_filename().encode('utf-8', 'replace')
                    except:
                        # If in default, it encoded, decode it! :)
                        filename = part.get_filename().decode('utf-8', 'replace')
                
                # Check prefix is exists or not
                if prefix is None:
                    prefix = str(int(time.time()))
                    
                # generate file name. Concat filename with prefix
                filename = ( prefix + '_').encode('utf-8', 'replace') + decode_header(filename)[0][0]
                filename = filename.replace('/', '-')
                file_ext = filename.split('.')[-1]
                
                # Classify filetype and save to folder
                if file_ext.lower() in ['jpg', 'png', 'gif', 'bmp', 'jpeg']:
                    location += '/image'
                elif file_ext.lower() in ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'dot', 'pdf']:
                    location += '/docs'
                elif file_ext.lower() in ['7z', 'gz', 'tar', 'zip', 'rar']:
                    location += '/archive'
                elif file_ext.lower() in ['mp3', 'wav', 'wma', 'm4a']:
                    location += '/sound'                
                elif file_ext.lower() in ['mp4', 'wmv', 'avi', 'mov']:
                    location += '/video'                               
                elif file_ext.lower() in ['dat']:
                    location += '/data'                
                
                if not os.path.exists(location):
                    os.makedirs(location, 0744)
                    
                attachment_list.append(location + '/' + filename)
                
                with open(location + '/' + filename, 'wb') as f:
                    f.write(part.get_payload(decode=True))
        return attachment_list
    
    #----------------------------------------------------------------------
    def get_body(self):
        """"""
        body = ''
        for part in self.message.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if not part.get_filename():
                if (part.get_payload(decode=True)):
                    body += part.get_payload(decode=True)        
                else:
                    print "Error get message"
        return body.decode('ascii', 'replace').encode('utf8')
    
    #----------------------------------------------------------------------
    def get_body_raw(self):
        """"""
        body = ''
        for part in self.message.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if not part.get_filename() and part.get_content_type() == 'text/plain':
                if (part.get_payload(decode=True)):
                    body += part.get_payload(decode=True)        
                else:
                    print "Error get message"
        return body.decode('ascii', 'replace').encode('utf8')    
    
    #----------------------------------------------------------------------
    def get_body_html(self):
        """"""
        body = ''
        for part in self.message.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if not part.get_filename() and part.get_content_type() == 'text/html':
                if (part.get_payload(decode=True)):
                    body += part.get_payload(decode=True)        
                else:
                    print "Error get message"
        return body.decode('ascii', 'replace').encode('utf8')           