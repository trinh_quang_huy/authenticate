from mongoengine import *
from ._lib.GoogleEmail import GoogleEmail
from django.db import models
from django.conf import settings
import email
import time
import os
# Create your models here.
class Label(EmbeddedDocument):
    content = StringField()
    text_color = StringField(default="#000000")
    background_color = StringField(default="#FFFFFF")

class GmailMessage(DynamicDocument):
    messageid = StringField(primary_key=True)
    threadid = StringField()    
    message_from = StringField()
    message_to = ListField(StringField())
    message_cc = ListField(StringField())
    message_bcc = ListField(StringField())
    subject = StringField()
    sent_date = IntField()
    attachments = ListField(StringField())
    body = StringField()
    body_formated = StringField()
    label = ListField(EmbeddedDocumentField(Label))
    priority = IntField(default=0)
    comment = StringField(default='')
    created_date = IntField(default=int(time.time()))
    
    #----------------------------------------------------------------------
    def process_message(self, message, att_path=settings.MEDIA_ROOT):
        """"""
        self.messageid = message['id']
        self.threadid = message['threadId']
        self.snippet = message['snippet']
        labels = []
        for label in message['labelIds']:
            label_obj = Label(content=label)            
            labels.append(label_obj)
        self.label = labels
        gmail_extractor = GoogleEmail(message['raw'])
        header = gmail_extractor.extract_header()
        
        self.message_from = header['from']
        self.message_to = header['to']
        self.message_cc = header['cc']
        self.message_bcc = header['bcc']
        self.subject = header['subject']
        self.sent_date = header['date']
        
        if not os.path.exists(att_path):
            os.makedirs(att_path, 0744)
        attachments = gmail_extractor.save_attachments(att_path , prefix=self.messageid)
        self.attachments = attachments
        self.body = gmail_extractor.get_body_raw()
        self.body_formated = gmail_extractor.get_body_html()
        
        
        
    
    
