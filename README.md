# README #

Labrador is a project to create a CMS manage Oauth of some providers as Google, Facebook, Twitter...

This is the version 2.0. Using Django framework.

## SETUP ##
REQUIREMENT: Your platform must have python language and installed [pip](https://pypi.python.org/pypi/pip)

* Clone this code to local

```
git clone git@bitbucket.org:hunterx/labrador.git
```

* Install Django
```
pip install django
```

* Install MongoDB Engine

This module is a alternative of default model of Django. It use MongoDB as main database. For more details, read full document at [http://docs.mongoengine.org/en/latest/guide/installing.html](http://docs.mongoengine.org/en/latest/guide/installing.html)
```
$ pip install mongoengine
```

* Install Mongodb as a database. In this project version, we use MongoDB as the main database. Click this [link](https://www.mongodb.org/downloads) for details of installation 

### Configuration ###
Open file `labrador/setting.py` and edit follow variables suitable with your platform:

* STATICFILES_DIRS
* STATIC_ROOT
* MEDIA_ROOT

### Setup schedule crawl ###
* With Linux platform

Linux using cron as a schedule system. You can setup a schedule with reference of this website [http://www.cronmaker.com/](http://www.cronmaker.com/)

