from django.shortcuts import render
from django.http import JsonResponse, response
from django.core.files.storage import FileSystemStorage, Storage
from django.conf import settings
from django.core.urlresolvers import reverse
from victims.models import Victims
import django.utils.text as utils_text

import models as serviceModel


# Create your views here.
#----------------------------------------------------------------------
def index(request):
    data = []
    for service in serviceModel.Services.objects:
        service.num_victim = len(Victims.objects(provider=service.slug))
        data.append(service)
    
    return render(request, 'index.html', {'data': data, 'menu':'service'})

#----------------------------------------------------------------------
def add(request):

    # split string to get provider
    postfix = 'Services'    
    
    # get all providers class
    provider_list = serviceModel.Services()._subclasses
    providers = []
    
    # check if provider class is base class
    for provider in provider_list:
        if provider == postfix:
            continue        
        providers.append(provider.split('.')[1])
    return render(request, 'create.html', {'providers':providers, 'menu':'service'})

#----------------------------------------------------------------------
def store(request):
    if request.method != 'POST':
        return response.HttpResponseForbidden()

    provider_name = request.POST['provider']
    
    try:
        provider = getattr(serviceModel, provider_name)
    except Exception:
        print "Can't not fetch provider class."
        
    # Instance provider model class
    service_obj = provider()

    service_obj.validate_n_process(request)
                        
    service_obj.save(force_insert=True)
    return response.HttpResponseRedirect('/services')

#----------------------------------------------------------------------
def update(request, service_slug=''):
    """"""
    service = serviceModel.Services.objects(slug=service_slug).first()
    if not service:
        return response.HttpResponseRedirect('/services')       
    if request.method == 'GET':
             
        
        # split string to get provider
        postfix = 'Services'    
        
        # get all providers class
        provider_list = serviceModel.Services()._subclasses
        providers = []
        
        # check if provider class is base class
        for provider in provider_list:
            if provider == postfix:
                continue        
            providers.append(provider.split('.')[1])        
        
        return render(request, 'edit.html', {'service': service, 'providers':providers, 'menu':'service'})
    else:        
            
        service.validate_n_process(request, update=True)
        service.save()
        return response.HttpResponseRedirect(reverse('services-index'))

#----------------------------------------------------------------------
def delete(request):
    """"""
    if request.method == 'POST':
        slug = request.POST['slug']
        service = serviceModel.Services.objects(slug=slug)        
        if service:
            service.delete()
            return JsonResponse({'status': "OK", 'element': slug})
        else:
            return JsonResponse({'status': "Fail"})
    
#----------------------------------------------------------------------
def ajax_provider_fields(request):
    
    if request.method != 'POST':
        return response.HttpResponseBadRequest()
    
    provider = request.POST['provider']    
    fields = {}
    service = None
    provider = getattr(serviceModel, provider)    
    
    if request.POST.__contains__('service_slug'):
        service = serviceModel.Services.objects(slug=request.POST['service_slug']).first()
        
    form_fields = provider().form_fields
    for field in form_fields:
        element = form_fields[field]        
        if (element.has_key('input_type')  and element['input_type'] == 'text') and (service is not None and service.__contains__(element['input_name'])):
            element['input_value'] = service[element['input_name']]
        fields[field] = element
                
    
    
    return JsonResponse(fields)
    