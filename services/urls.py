from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='services-index'),
    url(r'^add$', views.add, name='service-add'),
    url(r'^store$', views.store, name='service-store'),
    url(r'^update/(?P<service_slug>[a-z0-9-]+)/$', views.update, name='service-update'),
    url(r'^delete$', views.delete, name='service-delete'),
    url(r'^ajax-provider-fields$', views.ajax_provider_fields, name='service-ajax-provider-fields'),
]

