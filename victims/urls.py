from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='victim-index'),    
    url(r'^(?P<provider_name>[a-z0-9-]+)/add/$', views.add, name='victim-add'),    
    url(r'^(?P<provider_name>[a-z0-9-]+)/list/$', views.list_all, name='victim-list'),    
    url(r'^(?P<provider_name>[a-z0-9-]+)/edit/(?P<email_hash>[a-zA-Z0-9\+\/=]+)$', views.update, name='victim-edit'),    
    url(r'^(?P<provider_name>[a-z0-9-]+)/resource/(?P<email_hash>[a-zA-Z0-9\+\/=]+)/(?P<resource_id>[0-9a-zA-Z]+)$', views.detail_resource, name='victim-detail-resource'),    
    url(r'^(?P<provider_name>[a-z0-9-]+)/resource/(?P<email_hash>[a-zA-Z0-9\+\/=]+)$', views.view_resource, name='victim-resource'),    
    url(r'^(?P<provider_name>[a-z0-9-]+)$', views.direct_crawl, name='victim-directed-crawl'),    
    
    url(r'^delete$', views.delete, name='victim-delete'),    
    url(r'^import$', views.import_csv, name='victim-import'),    
]

