#!/usr/bin/env python
#coding:utf-8
"""
  Author:  Thanh Dancer --<>
  Purpose: Schedule crawl. Enter the hour of frequency (1 day = 24). 
  System will auto find victim who has same frequency and actived and crawl email with 
  the frequency setted
  
  Created: 09/16/2015
"""
from django.core.management.base import BaseCommand, CommandError

from victims.models import Victims
from mongoengine import *
import mongoengine
import argparse

########################################################################
class Command(BaseCommand):
    """"""
    help = "Schedule crawl victim resource by hour"

    #----------------------------------------------------------------------
    def add_arguments(self, parser):
        """"""
        parser.add_argument('hour', nargs='+', type=int)
    
    #----------------------------------------------------------------------
    def handle(self, *args, **options):
        """"""
        # Convert hour to second. This action is fit with victim model
        for input_hour in options['hour']:            
            hour2second = input_hour * 3600
            
            # Fetch object with conditions
            try:
                victim_list = Victims.objects(active=True, frequency=hour2second)
            except  Exception,e:
                self.stdout.write("Error: %s" % str(e)) 
            else:
                # Start crawling
                for victim in victim_list:
                    self.stdout.write("Crawling victim with email=%s" % victim.email)                     
                    victim.provider.get_resource(victim)        