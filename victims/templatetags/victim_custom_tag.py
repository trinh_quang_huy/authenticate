from django import template
from datetime import datetime, timedelta
import time
import base64

register = template.Library()

@register.filter(name='second_to_year')
def second_to_other_unit(value):
    date_data = datetime(1,1,1) + timedelta(seconds=int(value))
    #  return years, months, weeks, days, hours, minutes, seconds
    if date_data.year > 1:
        return "%d years" % (date_data.year - 1)
    elif date_data.month > 1:
        return "%d months" % (date_data.month - 1)
    elif date_data.day > 7:
        return "%d weeks" % (date_data.day / 7)
    elif date_data.day > 1:
        return "%d days" % (date_data.day -1)
    else:
        return "%d hours, %d minutes, %d seconds" % (date_data.hour, date_data.minute, date_data.second)

@register.filter(name='second_since_now')
def second_since_now(value):
    return second_to_other_unit(str(int(time.time()) - int(value)))

@register.filter
def base64_encode(value):
    return base64.b64encode(value)

@register.filter
def unixtime_to_human(value):
    return datetime.fromtimestamp(int(value))